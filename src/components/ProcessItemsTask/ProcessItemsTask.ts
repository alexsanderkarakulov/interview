/*
    1 часть
 Нужно выполнить функцию processor для каждого элемента
 processTime нужно добавить в каждый элемент результирующего массива

    2 часть
 Если processTime больше PROCESS_TIME_LIMIT - элемент нужно исключить из результирующего массива

    3 часть
 Элементы в результирующем массиве должны быть в том же порядке, что и в оригинальном
*/

type AsyncProcessor = (item: Item) => Promise<{processTime: number}>;
type Item = Record<string, string>;
type RichItem = Item & {processTime: number};

const PROCESS_TIME_LIMIT = 1000;

async function processItems(items: Item[], processor: AsyncProcessor): Promise<RichItem[]> {

    //... твой код :)
    return
}

/*
    Пример входных данных: [{id: '0'}, {id: '1'}, {id: '3'}]

    Пример выходных данных: [{id: '0', processTime: 123 }, {id: '3', processTime: 999}]
 */