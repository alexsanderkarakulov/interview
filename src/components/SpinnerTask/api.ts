export type Post = {
  userId: number,
  id: number,
  title: string,
  body: string
}

export function fetchPosts(): Promise<Post[]> {
  return fetch('https://jsonplaceholder.typicode.com/posts').then(res => res.json());
}